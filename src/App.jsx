import { Routes, Route } from "react-router-dom";
import { QueryClient, QueryClientProvider } from 'react-query'
import Navbar from "@/components/Navbar/Navbar";
import HomePage from "@/pages/HomePage/HomePage";
import CartPage from '@/pages/CartPage/CartPage';
import ErrorPage from '@/pages/ErrorPage/ErrorPage';
import CartContextWrapper from '@/context/CartContext';
import classes from './app.module.css';
import 'bootstrap/dist/css/bootstrap.min.css';

const queryClient = new QueryClient()

const App = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <CartContextWrapper>
        <Navbar />
        <div className={classes.root}>
          <div className={classes.container}>
            <Routes>
              <Route path="/" element={<HomePage />} />
              <Route path="/cart" element={<CartPage />} />
              <Route path="/error" element={<ErrorPage />} />
            </Routes>
          </div>
        </div>
      </CartContextWrapper>
    </QueryClientProvider>
  )
}

export default App
