import { useContext } from 'react';
import axios from 'axios';
import classes from './homePage.module.css';
import { useQuery } from 'react-query';
import { Navigate } from 'react-router-dom';
import { Row, Col, Container, Button } from 'react-bootstrap';
import constants from '@/constants/constants';
import ProductCard from '@/components/ProductCard/ProductCard';
import { CartContext } from '@/context/CartContext';

const HomePage = () => {
  const { isLoading, error, data: result } = useQuery("products", () =>
    axios.get(`${constants.SERVER_URL}/products`)
  );

  const { cartProducts, setCartProducts } = useContext(CartContext);

  return (
    <Container className={classes.root}>
      {
        error ? <Navigate to={'/error'} /> :
          isLoading ? 'Loading...' :
            <Row>
              {
                result.data.map(product => {
                  const isInCart = cartProducts.find(cartProduct => cartProduct.title === product.title);
                  return (
                    <Col xs={4} key={product.id} style={{ marginBottom: '20px' }}>
                      <ProductCard
                        {...product}
                        actionButton={
                          isInCart ?
                            <Button disabled>
                              In Cart
                            </Button> :
                            <Button variant="primary" onClick={() => setCartProducts([...cartProducts, product])}>
                              Add To Cart
                            </Button>
                        }
                      />
                    </Col>
                  )
                })
              }
            </Row>
      }
    </Container>
  )
}

export default HomePage
