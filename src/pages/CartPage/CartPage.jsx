import { useContext, useCallback, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import constants from '@/constants/constants';
import classes from './cartPage.module.css';
import { Row, Col, Container, Button, Alert } from 'react-bootstrap';
import ProductCard from '@/components/ProductCard/ProductCard';
import { CartContext } from '@/context/CartContext';

const CartPage = () => {
    const navigate = useNavigate();
    const [showAlert, setShowAlert] = useState(false);
    const [isSuccess, setIsSuccess] = useState(false);
    const { cartProducts, setCartProducts } = useContext(CartContext);
    const buyProducts = useCallback(() => {
        axios.post(`${constants.SERVER_URL}/products/buy`, cartProducts).then(() => {
            setIsSuccess(true);
        }).catch(() => {
            setIsSuccess(false);
        }).finally(() => {
            setShowAlert(true);
            setTimeout(() => {
                setShowAlert(false);
                setCartProducts([]);
                navigate('/');
            }, 2000)
        })
    }, [cartProducts])

    return (
        <>
            {
                cartProducts.length === 0 ?
                    <Container className={classes.emptyRoot}>
                        <h2>Looks like there is no items in your cart.</h2>
                    </Container> :
                    <Container className={classes.root}>
                        <Row>
                            {
                                cartProducts.map(product => (
                                    <Col xs={4} key={product.id}>
                                        <ProductCard
                                            className={classes.productCard}
                                            {...product}
                                            actionButton={
                                                <Button
                                                    variant="danger"
                                                    onClick={() =>
                                                        setCartProducts(cartProducts.filter(cartProduct => cartProduct.id !== product.id))}
                                                >
                                                    Remove From Cart
                                                </Button>
                                            }
                                        />
                                    </Col>
                                ))
                            }
                        </Row>
                        <div className={classes.summary}>
                            <h3>Total Products: {cartProducts.length}</h3>
                            <h3>
                                Total Price: {cartProducts.map(product => product.price)
                                    .reduce((a, b) => a + b)}₪
                            </h3>
                            <Button
                                variant="primary"
                                onClick={buyProducts}
                            >
                                Buy
                            </Button>
                        </div>
                        {
                            showAlert &&
                            <Alert variant={isSuccess ? 'success' : 'danger'}>
                                {isSuccess ? 'Succesfully buying the wanted products!' : 'We could not handle your buying right now...'}
                            </Alert>
                        }
                    </Container>
            }
        </>
    )
}

export default CartPage
