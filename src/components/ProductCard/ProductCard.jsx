import { Card, Button } from 'react-bootstrap';

const ProductCard = ({ title, price, description, imageUrl, actionButton }) => {
    return (
        <Card>
            <Card.Img variant="top" src={imageUrl} style={{ height: '20vh' }} />
            <Card.Body>
                <Card.Title>{title} - {price}₪</Card.Title>
                <Card.Text>
                    {description}
                </Card.Text>
                {actionButton}
            </Card.Body>
        </Card>
    );
}

export default ProductCard;